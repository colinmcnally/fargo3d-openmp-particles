#include <errno.h>
#include <float.h>
#include "fargo3d.h"
#include "particles.h"

//
// This default needs to be overridden in your setup with something that makes sense for you physics and units
//

void particle_drag_law(double radius, double mass, double gasdens, double cs, double deltavel, double* tstop, double* vpower){
  //this must be threadsafe!
  double sigma_mol;
  double mu_mol;
  double lambda_mfp;
  double mu_visc;
  double reynolds;
  double mach;
  double knudsen;
  double cD_eps;
  double cD_stokes;
  double cD;
  double rho_dust;

  //Use the Nelson & Gressel 2010 parameterization of drag, as it has nice power laws in the drift velocity!
  //    2010MNRAS.409..639N
  //
  //  Also inspired by:
  //Lyra, W., Johansen, A., Klahr, H., & Piskunov, N. (2008, October 17). 
  //http://doi.org/10.1051/0004-6361:200810797
  //
  // and Woitke & Helling 2003 2003A&A...399..297W
  //
  sigma_mol = 9.38e-42; //collisional cross section of H2 2.1e-15 cm^2 in au^2 (Woitke & Helling)
  mu_mol = 1.96e-57;    // mean molecular mass of 5:1 H2:He mixture is 3.9e-24 g, in Msun (Woitke & Helling)
  lambda_mfp = mu_mol/(gasdens*sigma_mol);
  mu_visc = sqrt(8.0/M_PI)*gasdens*cs*lambda_mfp/3.0; //kinematic viscosity
  reynolds = 2.0*radius*gasdens*deltavel/mu_visc;
  mach = deltavel/cs;
  knudsen = lambda_mfp/(2.0*radius);

//Woitke & Helling
//  cD_eps = 2.0*sqrt(1.0+128.0/(9.0*M_PI*mach*mach)); // Lyra's Kwok 1975 form
//  if (reynolds <= 500.0){
//    cD_stokes = 24.0/reynolds+3.6*pow(reynolds,-0.313);
//  }else if (reynolds <= 1500.0){
//    cD_stokes = 9.5e-5*pow(reynolds,1.397);
//  }else{
//    cD_stokes = 2.61;
//  }
//  cD = (9.0*knudsen*knudsen*cD_eps + cD_stokes) / ((3.0*knudsen +1.0)*(3.0*knudsen +1.0));
//
//  rho_dust = mass/(4.0*M_PI/3.0*radius*radius*radius);
//  tstop = 4.0*lambda_mfp*rho_dust/(3.0*gasdens*cD*cs)*1.0/(mach*knudsen);

  // Nelson & Gressel 2010
  rho_dust = mass/(4.0*M_PI/3.0*radius*radius*radius);
  // solve for the right changeover vel, where the two drags are equal in the subsonic limit.
  if (radius < lambda_mfp ){ //Epstien regime
    cD = 2.0; // low mach number limit, above this the power law dep is nasty. see the version above. Oliver has something in Nirvana3.5 however
    *vpower = 1.0;
    *tstop = rho_dust * radius / (gasdens * cs); // don't use fd and cD, shortcut that.
  }else{ // Stokes regime
    cD = 0.0;
    *vpower = 1.0;
    if ( reynolds > 0.0 && reynolds < 1.0){
      cD = 24.0/reynolds;
      *vpower = 1.0;
    }else if ( reynolds >= 1.0 && reynolds < 1.0 ){
      cD = 24.0*pow(reynolds,-0.6);
      *vpower = 1.4;
    }else{
      cD = 0.44;
      *vpower = 2.0;
    }
    // tstop = (drag_force/mass * drift velocity^-1 )^-1
    // drag_force = 0.5 * cD * pi * radius^2 * gasdens * abs(drift velocity) * drift velocity
    *tstop = 1.0/(0.5* cD * M_PI * radius*radius * gasdens *deltavel / mass);
  }
  
  return;
}
