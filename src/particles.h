//name of the particle log file - high freq output
#define PARTICLELOG "particlelog.txt"
#define PARTICLELOGBIN "particlelog.dat"

// Factor for how much relative accel to allow per timestep
#define PARTICLE_ACC_TSTEP_FACTOR 0.5

/////////
// centerings for interpolated quantities
#define XFACE 0
#define YFACE 1
#define ZFACE 2
#define CELLCENTER 3

#define LOGSTART  1
#define LOGAPPEND 2

#define STAGE_PREDICT 1
#define STAGE_CORRECT 2

// particle information
long long int nparticles; //total number of particles, gives array sizes, 64 bits

long long int *pid; //particle ID, 64 bits

double *mass; // 64 bit types
double *radius;
double *acceleration_tstep;

double *posx; //poisitions, and temps in th 0 arrays - these should be a tidier data structure
double *posy;
double *posz;

double *velx;
double *vely;
double *velz;

double *velx0;
double *vely0;
double *velz0;

double *tstop;
int    *ownedby;

double *syncbuf; 
int *syncbufint;

double dt; // the dt for each step, set by the call to predict, used by correct

double particle_tstep; // used by algogas to limit the timestep

// prototypes
void particles_init(int);
void particles_snapshot(int);
void particles_logfile(); //text version
void particles_logfile_bin(); //binary version
void particles_getaccel(int);
void particles_predict(double);
void particles_correct();

int loc_on_this_proc( double, double, double);
void get_field_index( double, double, double, int*, int*, int*);
void get_gas_velocity_density(double, double, double, double*, double*, double*, double*, double*);
double interpolate_field_loc(real*, int, double, double, double, int, int, int);
void sync_particle_array( double* );
void sync_particle_array_int( int* );
double xgmap(double);
void get_planet_accel_on_particle(int, double, double, double, double*, double*, double*);
void set_particle_tstep();
// in a separate file
void particle_drag_law(double, double, double, double, double, double*, double*);
