#include <errno.h>
#include <float.h>
#include "fargo3d.h"
#include "particles.h"


/*
 * Things which might change:
 * - uses trilinear interpolation for gas velocities, CIC might be more standard
 * - awkward way of doing the gas drag function, and the central potential, sort of inherent to FARGO3D
 */

void particles_init(int snapshot){
  FILE *input;
  char name[512];
 
  int idbg;
  int ii;

  MPI_Barrier(MPI_COMM_WORLD);
  printf("in particles_init on rank %i\n",CPU_Rank);
  //run after gas init (condinit.c)
  //on the root process
  if (CPU_Master){
    //open a snapshot
    sprintf(name, "%ssnapparticles_%i.dat", OUTPUTDIR, snapshot);
    input = fopen(name, "rb");  //read binary
    if (input == NULL){
      fprintf(stderr,"Could not open particle initial condition %s\n",name);
      prs_exit(EXIT_FAILURE);
    }

    //read the number of particles as the first value
    errno = 0;
    if(fread(&nparticles, sizeof(long long int), 1, input) != 1){
       fprintf(stderr,"Read particles nparticles got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    printf("Will read %lli particles\n",nparticles);
  }
  //communicate the size of the arrays
  MPI_Bcast(&nparticles, 1, MPI_LONG_LONG_INT, 0, MPI_COMM_WORLD);
  //allocate the particle arrays
  pid  = malloc(nparticles*sizeof(long long int));
  mass = malloc(nparticles*sizeof(double));
  radius = malloc(nparticles*sizeof(double));
  posx = malloc(nparticles*sizeof(double));
  posy = malloc(nparticles*sizeof(double));
  posz = malloc(nparticles*sizeof(double));
  velx = malloc(nparticles*sizeof(double));
  vely = malloc(nparticles*sizeof(double));
  velz = malloc(nparticles*sizeof(double));
  acceleration_tstep = malloc(nparticles*sizeof(double));
  ownedby = malloc(nparticles*sizeof(int));
  velx0 = malloc(nparticles*sizeof(double));
  vely0 = malloc(nparticles*sizeof(double));
  velz0 = malloc(nparticles*sizeof(double));
  tstop = malloc(nparticles*sizeof(double));

  syncbuf = malloc(nparticles*sizeof(double));
  syncbufint = malloc(nparticles*sizeof(int));

  if (CPU_Master){
    //read particle positions and velocitites from the snapshot.
    errno = 0;
    printf("nparticles %lli\n",nparticles);
    if(fread(pid, sizeof(long long int), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles pid got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(mass, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles mass got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(radius, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles radius got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(posx, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles posx got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(posy, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles posy got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(posz, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles posz got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(velx, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles velx got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(vely, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles vely got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(velz, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles velz got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(acceleration_tstep, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles acceleration_tstep got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fread(tstop, sizeof(double), nparticles, input) != nparticles){
       fprintf(stderr,"Read particles tstop got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    //close the input file
    fclose(input);
  }
  //communicate particle position arrays
  MPI_Bcast(pid,  nparticles, MPI_LONG_LONG_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(mass, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(radius, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(posx, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(posy, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(posz, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(velx, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(vely, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(velz, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(acceleration_tstep, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(tstop, nparticles, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (ii=0; ii<nparticles; ii++){
    ownedby[ii] = 0;
    if ( loc_on_this_proc( xgmap(posx[ii]), posy[ii], posz[ii])) {
      ownedby[ii] = CPU_Rank+1; // so anything with 0 is not owned
    }
  }
  sync_particle_array_int(ownedby);

  // set the particle acceleration timestep limit for the next step
  set_particle_tstep();

  if(CPU_Rank==0){
    printf("particles are on rank 0\n");
    for (idbg=0; idbg<nparticles; idbg++){
      printf("%lli %e %e %e %e %e %e %e\n",pid[idbg],posx[idbg],posy[idbg],posz[idbg],
              velx[idbg],vely[idbg],velz[idbg],acceleration_tstep[idbg]);
    }
  }

}

void particles_finalize(){
  free(pid);
  free(mass);
  free(radius);
  free(posx);
  free(posy);
  free(posz);
  free(velx);
  free(vely);
  free(velz);
  free(acceleration_tstep);
  free(ownedby);
  free(velx0);
  free(vely0);
  free(velz0);
  free(tstop);
  free(syncbuf);
  free(syncbufint);
}

void particles_snapshot(int snapshot){
  FILE *output;
  char name[512];

  if (CPU_Master){
    //open a snapshot
    sprintf(name, "%ssnapparticles_%i.dat", OUTPUTDIR, snapshot);
    output = fopen(name, "wb");  //write binary
    if (output == NULL){
      fprintf(stderr,"Could not open %s\n",name);
      prs_exit(EXIT_FAILURE);
    }  
    //write the number of particles
    errno = 0;
    if(fwrite(&nparticles, sizeof(long long int), 1, output) != 1){
       fprintf(stderr,"Write particles nparticles got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    //write a series of records
    //structure: int particle_id, double mass, double radius, double x, double y, double z, double vx, double vy, double vz
    if(fwrite(pid, sizeof(long long int), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles pid got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(mass, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles mass got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(radius, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles radius got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(posx, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles posx got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(posy, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles posy got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(posz, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles posz got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(velx, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles velx got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(vely, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles vely got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(velz, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles velz got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(acceleration_tstep, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles acceleration_tstep got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    if(fwrite(tstop, sizeof(double), nparticles, output) != nparticles){
       fprintf(stderr,"Write particles acceleration_tstop got: %s\n",strerror(errno));
       prs_exit(EXIT_FAILURE);
    }
    //close file
    fclose(output);
  }// only master writes
}

//not threadsafe
void particles_logfile_bin(int logmode){
  // append a line to the logfile, one per particle, with the diagnostic output.
  // particle_id, double mass, double radius, double x, double y, double z, double vx, double vy, double vz, double tau_stop 
  FILE *logfile;
  char name[512];
  int ii;
  long long int magic= 42;
  if (CPU_Master){
    sprintf(name, "%s%s", OUTPUTDIR, PARTICLELOGBIN);
    
    if (logmode == LOGSTART){
      logfile = fopen(name, "wb");  //write binary
    }else{
      logfile = fopen(name, "ab");  
    }
    for (ii=0; ii<nparticles; ii++){
      fwrite(&PhysicalTime, sizeof(real), 1, logfile);
      fwrite(&pid[ii], sizeof(long long int), 1, logfile);
      fwrite(&mass[ii], sizeof(double), 1, logfile);
      fwrite(&radius[ii], sizeof(double), 1, logfile);
      fwrite(&posx[ii], sizeof(double), 1, logfile);
      fwrite(&posy[ii], sizeof(double), 1, logfile);
      fwrite(&posz[ii], sizeof(double), 1, logfile);
      fwrite(&velx[ii], sizeof(double), 1, logfile);
      fwrite(&vely[ii], sizeof(double), 1, logfile);
      fwrite(&velz[ii], sizeof(double), 1, logfile);
      fwrite(&tstop[ii], sizeof(double), 1, logfile);
      fwrite(&magic, sizeof(long long int), 1, logfile);

//      fprintf(logfile,"%e %lli %e %e %e %e %e %e %e %e %e\n",
//                PhysicalTime, pid[ii], mass[ii], radius[ii], 
//                posx[ii], posy[ii], posz[ii], velx[ii], vely[ii], velz[ii], tstop[ii]);

//       fprintf(logfile,"%e %lli %e %e %e %e %e %e %e %e %e %e %e\n",
//                PhysicalTime, pid[ii], mass[ii], radius[ii], 
//                posx[ii], posy[ii], posz[ii], velx[ii], vely[ii], velz[ii], accx0[ii], accy0[ii], accz0[ii]);
    }
    fclose(logfile); 
  } // end CPU_Master
}

//not threadsafe
void particles_logfile(int logmode){
  // append a line to the logfile, one per particle, with the diagnostic output.
  // particle_id, double mass, double radius, double x, double y, double z, double vx, double vy, double vz, double tau_stop 
  FILE *logfile;
  char name[512];
  int ii;
  if (CPU_Master){
    sprintf(name, "%s%s", OUTPUTDIR, PARTICLELOG);
    
    if (logmode == LOGSTART){
      logfile = fopen(name, "w");  //write text 
    }else{
      logfile = fopen(name, "a");  
    }
    for (ii=0; ii<nparticles; ii++){
      fprintf(logfile,"%e %lli %e %e %e %e %e %e %e %e %e\n",
                PhysicalTime, pid[ii], mass[ii], radius[ii], 
                posx[ii], posy[ii], posz[ii], velx[ii], vely[ii], velz[ii], tstop[ii]);
//       fprintf(logfile,"%e %lli %e %e %e %e %e %e %e %e %e %e %e\n",
//                PhysicalTime, pid[ii], mass[ii], radius[ii], 
//                posx[ii], posy[ii], posz[ii], velx[ii], vely[ii], velz[ii], accx0[ii], accy0[ii], accz0[ii]);
    }
    fclose(logfile); 
  } // end CPU_Master
}



//threadsafe
int loc_on_this_proc(double locx, double locy, double locz){
  // return 0 (false) is the particle is outside the bound of this proc's grid
  int here = 1;
  if (!((locx >= XMIN) && (locx <= XMAX))) here = 0;
  if (!((locy >= ymin(NGHY)) && (locy <= ymin(Ny+NGHY)))) here = 0;
  #ifdef Z
  if (!((locz >= zmin(NGHZ)) && (locz <= zmin(Nz+NGHZ)))) here = 0;
  #endif
  //if ((locy > YMAX) || (locy < YMIN)) printf("loc_on_this_proc:  %e %e %e is outside the grid\n", locx, locy, locz);
  if (!((locx >= XMIN) && (locx <= XMAX))) {
    printf("loc_on_this_proc:  %e %e %e is outside the grid\n", locx, locy, locz);
    exit(0);
  }
  return here;
}


//threadsafe
void get_gas_velocity_density(double locx, double locy, double locz, 
                              double* velx, double* vely, double* velz, double* dens, double* cs_or_temp){
  int indx, indy, indz;
  double fielddens, scaleheight;
  get_field_index( locx, locy, locz, &indx, &indy, &indz);
  // we can now interpolate from any field to the location 
  *velx = interpolate_field_loc(Vx->field_cpu, XFACE, locx, locy, locz, indx, indy, indz);
  *velx += OMEGAFRAME*locy;
  *vely = interpolate_field_loc(Vy->field_cpu, YFACE, locx, locy, locz, indx, indy, indz);
  #ifdef Z
  *velz = interpolate_field_loc(Vz->field_cpu, ZFACE, locx, locy, locz, indx, indy, indz);
  #else
  *velz = 0.0;
  #endif
  *cs_or_temp = interpolate_field_loc(Energy->field_cpu, CELLCENTER, locx, locy, locz, indx, indy, indz);
  fielddens = interpolate_field_loc(Density->field_cpu, CELLCENTER, locx, locy, locz, indx, indy, indz);
  #ifdef Z
  *dens = fielddens;
  #else //ifdef Z
  // We're in 2D. Get the midplane gas density
  #ifdef ISOTHERMAL
  //assumes cylindrical, radial isothermal, etc...
  scaleheight = ASPECTRATIO*pow(locy/R0,FLARINGINDEX)*locy;
  *dens = fielddens/(sqrt(2.0*M_PI)*scaleheight);
  #else
  //NOT IMPLEMENTED
  *dens = 1e80;
  #endif // ifdef ISOTHERMAL
  #endif // ifdef Z
}

//threadsafe
void get_field_index(double locx, double locy, double locz, int *indexx, int *indexy, int *indexz){
  //routine returns the indicies of the first xmin above x location, ymin above y location, zmin abouve z location
  // so thath's theindicies one above the one the particle is in
  int indx, indy, indz;
  //sanity check is the particle is on this proc's grid
  if (!((locx >= xmin(0)) && (locx <= xmin(Nx))))
    printf("locx %e is outside bounds %e %e\n",locx, xmin(0), xmin(Nx));
  if (!((locy >= ymin(NGHY)) && (locy <= ymin(Ny+NGHY+1))))
    printf("locy %e is outside bounds %e %e\n",locy, ymin(NGHY), ymin(Ny+NGHY+1));
  #ifdef Z
  if (!((locz >= zmin(NGHZ)) && (locz <= zmin(NZ+NGHZ+1))))
    printf("locz %e is outside bounds %e %e\n",locz, zmin(NGHZ), zmin(Nz+NGHZ));
  #endif
  // find the first x axis index above locx 
  indx=0;
  while ((xmin(indx)<locx) && (indx < Nx)) indx++;
  indx--; // while loop went one past, will always be executed once if particle is inside domain
  indy=NGHY;
  while ((ymin(indy)<locy) && (indy < Ny+NGHY)) indy++;
  indy--;
  indz=NGHZ;
  #ifdef Z
  while ((zmin(indz)<locz) && (indz < Nz+NGHZ)) indz++;
  indz--;
  #endif // Z
  *indexx = indx;
  *indexy = indy;
  *indexz = indz;
}


//threadsafe
double interpolate_field_loc(real *field, int dir, double locx, double locy, double locz, int ix, int iy, int iz){
  // Bilinear interpolation - the minimum needed to get inspiralling particles right
  int i; //Variables reserved
  int j; //for the topology
  int k; //of the kernels
  int ll;

  int ixmedleft, iymedleft, izmedleft; // index of cell with med below (left) of loc
  
  double inter_xleft, inter_xright;
  double inter_yleft, inter_yright;
  double inter_zleft, inter_zright;
  double nearest_value;

  ixmedleft = locx > xmed(ix) ? ix : ix - 1;
  if (ixmedleft<0) ixmedleft = Nx-1; // as X has no ghost zones, need to wrap addresses
  iymedleft = locy > ymed(iy) ? iy : iy - 1;
#ifdef Z
  izmedleft = locz > zmed(iz) ? iz : iz - 1;
#endif
 
  if (dir == XFACE){
    // for looking things up in fields, need to exploit the macro: #define l   ((i)+(j)*(Nx)+((k)*Stride))
    //   printf("interpolate field at %e %e %e %i %i %i\n", locx, locy, locz, ix,iy,iz);
    //printf("XFACE %e %e %e\n",xmin(closestxmin), ymed(closestymed), zmed(closestzmed));
    //   printf("XFACE ymed iy %e iy+1 %e\n", ymed(iymedleft), ymed(iymedleft+1));
    
#ifdef Z
    // need to interpolate twice in z, from zmeds
    i = ix;
    j = iymedleft;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];
    i = ix;
    j = iymedleft;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_yleft = ((zmed(izmedleft+1) - locz) * inter_zleft + (locz - zmed(izmedleft)) * inter_zright) 
                   / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ix;
    j = iymedleft + 1;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];

    i = ix;
    j = iymedleft + 1;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_yright = ((zmed(izmedleft+1) - locz) * inter_zleft + (locz - zmed(izmedleft)) * inter_zright) 
                   / (zmed(izmedleft+1) - zmed(izmedleft));
#else
    //2D bilinear interpolation. Interpolate twice in in y, then in x
    i = ix;
    j = iymedleft;
    k = iz;
    ll = l;
    inter_yleft = field[ll];

    i = ix;
    j = iymedleft + 1;
    k = iz;
    ll = l;
    inter_yright = field[ll];
#endif

    //interpolate in y
    inter_xleft = ((ymed(iymedleft+1)-locy) * inter_yleft + (locy-ymed(iymedleft)) * inter_yright) 
                   / (ymed(iymedleft+1)-ymed(iymedleft));

#ifdef Z
    // need to interpolate twice in z, from zmeds
    i = ix;
    j = iymedleft;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ix;
    j = iymedleft;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_yleft = ((zmed(izmedleft+1) - locz) * inter_zleft + (locz - zmed(izmedleft)) * inter_zright) 
                   / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ix;
    j = iymedleft + 1;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ix;
    j = iymedleft + 1;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_yright = ((zmed(izmedleft+1) - locz) * inter_zleft + (locz - zmed(izmedleft)) * inter_zright) 
                   / (zmed(izmedleft+1) - zmed(izmedleft));
#else
    // in x, we must use the recursive macro lxp to handle the periodic BC as there are no ghost zones!
    i = ix;//+1; use lxp to do this +1, that handles the periodic BC
    j = iymedleft;
    k = iz;
    ll = lxp; // a macro of i,j,k
    inter_yleft = field[ll];

    i = ix;//+1; use lxp to do this +1, that handles the periodic BC
    j = iymedleft + 1;
    k = iz;
    ll = lxp;
    inter_yright = field[ll];
#endif

    //interpolate in y
    inter_xright = ((ymed(iymedleft+1)-locy) * inter_yleft + (locy-ymed(iymedleft)) * inter_yright) 
                    / (ymed(iymedleft+1)-ymed(iymedleft));

    //interpolate in x
    nearest_value = ((xmin(ix+1)-locx) * inter_xleft + (locx-xmin(ix)) * inter_xright)/(xmin(ix+1)-xmin(ix));

  }else if (dir == YFACE){
#ifdef Z
    i = ixmedleft;
    j = iy;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iy;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_xleft = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ixmedleft;
    j = iy;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iy;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_xright = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));
#else
    i = ixmedleft;
    j = iy;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;//+1; use lxp to do this +1, that handles the periodic BC
    j = iy;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];
#endif

    inter_yleft = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

#ifdef Z
    i = ixmedleft;
    j = iy + 1;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iy + 1;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_xleft = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ixmedleft;
    j = iy + 1;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iy + 1;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_xright = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

#else
    i = ixmedleft;
    j = iy + 1;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;//+1; use lxp to do this +1, that handles the periodic BC
    j = iy + 1;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];
#endif

    inter_yright = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

    nearest_value = ((ymin(iy+1)-locy) * inter_yleft + (locy-ymin(iy)) * inter_yright)
                     / (ymin(iy+1) - ymin(iy));

  }else if (dir == ZFACE){
#ifdef Z
    // interpolate in x
    i = ixmedleft;
    j = iymedleft;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;
    j = iymedleft;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];

    inter_yright = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

    i = ixmedleft;
    j = iymedleft + 1;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;
    j = iymedleft + 1;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];

    inter_yleft = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

    // interpolate in y
    inter_zleft = ((ymed(iymedleft+1)-locy) * inter_yleft + (locy-ymed(iymedleft)) * inter_yright) 
                    / (ymed(iymedleft+1) - ymed(iymedleft));

    // interpolate in x
    i = ixmedleft;
    j = iymedleft;
    k = iz + 1;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;
    j = iymedleft;
    k = iz + 1;
    ll = lxp;
    inter_xright = field[ll];

    inter_yright = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

    i = ixmedleft;
    j = iymedleft + 1;
    k = iz + 1;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;
    j = iymedleft+1;
    k = iz + 1;
    ll = lxp;
    inter_xright = field[ll];

    inter_yleft = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1) - xmed(ixmedleft));

    // interpolate in y
    inter_zright = ((ymed(iymedleft+1)-locy) * inter_yleft + (locy-ymed(iymedleft)) * inter_yright) 
                    / (ymed(iymedleft+1) - ymed(iymedleft));

    nearest_value = ((zmin(iz+1)-locz) * inter_zleft + (locz-zmin(iz)) * inter_zright)
                     / (zmin(iz+1) - zmin(iz));

#else
    // never called, 
    i = ix;
    j = iy;
    k = iz;
    ll = l;
    nearest_value = field[ll];
#endif
  }else if (dir == CELLCENTER){ //not the cell value - the value at the nearest cell center!
#ifdef Z
    i = ixmedleft;
    j = iymedleft;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iymedleft;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_xleft = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ixmedleft;
    j = iymedleft;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iymedleft;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_xright = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));
#else
    i = ixmedleft;
    j = iymedleft;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;//+1; use lxp 
    j = iymedleft;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];
#endif

    inter_yleft = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                   / (xmed(ixmedleft+1) - xmed(ixmedleft));

#ifdef Z
    i = ixmedleft;
    j = iymedleft + 1;
    k = izmedleft;
    ll = l;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iymedleft + 1;
    k = izmedleft + 1;
    ll = l;
    inter_zright = field[ll];

    inter_xleft = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

    i = ixmedleft;
    j = iymedleft + 1;
    k = izmedleft;
    ll = lxp;
    inter_zleft = field[ll];

    i = ixmedleft;
    j = iymedleft + 1;
    k = izmedleft + 1;
    ll = lxp;
    inter_zright = field[ll];

    inter_xright = ((zmed(izmedleft+1)-locz) * inter_zleft + (locz-zmed(izmedleft)) * inter_zright) 
                    / (zmed(izmedleft+1) - zmed(izmedleft));

#else
    i = ixmedleft;
    j = iymedleft+1;
    k = iz;
    ll = l;
    inter_xleft = field[ll];

    i = ixmedleft;//+1; use lxp
    j = iymedleft+1;
    k = iz;
    ll = lxp;
    inter_xright = field[ll];
#endif

    inter_yright = ((xmed(ixmedleft+1)-locx) * inter_xleft + (locx-xmed(ixmedleft)) * inter_xright) 
                    / (xmed(ixmedleft+1)-xmed(ixmedleft));

    nearest_value = ((ymed(iymedleft+1)-locy) * inter_yleft + (locy-ymed(iymedleft)) * inter_yright)
                     / (ymed(iymedleft+1)-ymed(iymedleft));
  }
  return nearest_value;
}


void particles_kick(double dt){
  // this applies a kick to the particles based on their current position, of time size dt
  // this routine gets the acceleration at the particle current positions. This is the same operation, but involves 
  //  different memeory locations at each stage. Better data structures could make this routine much simpler.
  int ii;
  int iplanet, nplanets;
  double kickx, kicky, kickz;
  double paccx, paccy, paccz;
  double gasvx, gasvy, gasvz, gasdens, gascs_or_temp;
  double pvx, pvy, pvz;
  double px, py, pz;
  double deltavel;
  double this_tstop;
  double this_vpower;
  double stop_factor;

  // zero out the arrays that will be sync'd
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) tstop[ii] = 0.0;
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) velx0[ii] = 0.0;
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) vely0[ii] = 0.0;
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) velz0[ii] = 0.0;
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) acceleration_tstep[ii] = 0.0;
#pragma omp parallel for
  for (ii=0; ii<nparticles; ii++) ownedby[ii] = 0;
  

  // loop over all the particles
#pragma omp parallel for private(px, py, pz, pvx, pvy, pvz, gasvx, gasvy, gasvz, gasdens, gascs_or_temp, iplanet, nplanets, kickx, kicky, kickz, paccx, paccy, paccz, deltavel, this_tstop, this_vpower, stop_factor)
  for (ii=0; ii<nparticles; ii++){
    // if this particle lives on this processor, calculate accel and tau_stop for this particle
    // uses current velocity always.
    px = posx[ii];
    py = posy[ii];
    pz = posz[ii];
    pvx = velx[ii];
    pvy = vely[ii];
    pvz = velz[ii];

    if ( loc_on_this_proc( xgmap(px), py, pz)) {
      ownedby[ii] = CPU_Rank+1; // so anything with 0 is not owned
      //acceleration due to gas drag
      get_gas_velocity_density(xgmap(px), py, pz, &gasvx, &gasvy, &gasvz, &gasdens, &gascs_or_temp);
      deltavel = sqrt( (pvx-gasvx)*(pvx-gasvx) +(pvy-gasvy)*(pvy-gasvy) + (pvz-gasvz)*(pvz-gasvz) );
      particle_drag_law(radius[ii], mass[ii], gasdens, gascs_or_temp, deltavel, &this_tstop, &this_vpower);
      
      if (this_vpower == 1.0){
        // this is Nelson & Gressel 2010 eq 12 w mods
        stop_factor = 1.0 - exp(-dt/this_tstop);
      }else{
        // this is Nelson & Gressel 2010 eq 13 w mods
        stop_factor = 1.0 - pow( pow(deltavel,1.0-this_vpower) * (1.0 + (this_vpower-1.0)*this_tstop*dt) , 
                                 1.0/(1.0-this_vpower))/deltavel;
      }
      kickx = -(pvx - gasvx) * stop_factor;
      kicky = -(pvy - gasvy) * stop_factor;
      kickz = -(pvz - gasvz) * stop_factor;

      // accelerations due to the gravity of planets
      paccx = 0.0;
      paccy = 0.0;
      paccz = 0.0;
      if (Sys != NULL) {
        nplanets = Sys->nb;
        for (iplanet = 0; iplanet < nplanets; iplanet++) {
          get_planet_accel_on_particle(iplanet, xgmap(px), py, pz, &paccx, &paccy, &paccz);
        }
      }

      //central force and coordinate system terms
      // note velocities are all in linear units, not angular units
      // these terms are the derivatives of the basis unit vectors
      // A good way to test these, and the kick, is to turn off all forces and let particles go in straight lines
#ifdef CYLINDRICAL
      paccx += -vely[ii] * velx[ii] / posy[ii];
      paccy +=
#ifdef NODEFAULTSTAR
               // binary taken care of by planet sys potential
#else
               - (G*MSTAR/( posy[ii] * posy[ii])) 
#endif
               + velx[ii] * velx[ii] / posy[ii];
#ifdef Z
      NOT IMPLEMENTED
#else //Z
      paccz += 0.0;
#endif //Z

#elif defined(SPHERICAL)
      paccx += 1.0/posy[ii] * (- vely[ii] * velx[ii]  - velx[ii] * velz[ii] / tan(posz[ii]) );
      paccy += 
#ifdef NODEFAULTSTAR
               // binary taken care of by planet sys potential
#else
               - (G*MSTAR/( posy[ii] * posy[ii])) 
#endif
               + (velx[ii]*velx[ii] + velz[ii]*velz[ii]) / posy[ii];
      paccz += 1.0/posy[ii] * ( - velz[ii] * vely[ii] + velx[ii] * velx[ii] / tan(posz[ii]) );
#else
      NOT IMPLEMENTED
#endif

      kickx += dt * paccx;
      kicky += dt * paccy;
      kickz += dt * paccz;
      tstop[ii] = this_tstop;
      velx0[ii] = velx[ii] + kickx;
      vely0[ii] = vely[ii] + kicky;
      velz0[ii] = velz[ii] + kickz;
      acceleration_tstep[ii] = sqrt((velx[ii]*velx[ii] + vely[ii]*vely[ii] + velz[ii]*velz[ii])
                                      /(paccx*paccx + paccy*paccy + paccz*paccz));
    }
  }

  //sync the results, and copy back onto of the particle velocities
  sync_particle_array(velx0);
  sync_particle_array(vely0);
  sync_particle_array(velz0);
  sync_particle_array(tstop);
  sync_particle_array(acceleration_tstep);
  sync_particle_array_int(ownedby);
  memcpy( velx, velx0, nparticles*sizeof(double));
  memcpy( vely, vely0, nparticles*sizeof(double));
  memcpy( velz, velz0, nparticles*sizeof(double));
}

void sync_particle_array(double* target){
  MPI_Allreduce(target, syncbuf, nparticles, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  memcpy(target, syncbuf, nparticles*sizeof(double));
}

void sync_particle_array_int(int* target){
  MPI_Allreduce(target, syncbuf, nparticles, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  memcpy(target, syncbuf, nparticles*sizeof(int));
}


double xgmap(double xin){
  //map an angular coordinate back to the grid
  double modval;
  // this does not work --> return fmod(xin-(XMIN),XMAX-XMIN)+XMIN;
  // c's fmod is remainder, not the mathier version of modulus
  modval = fmod(xin-(XMIN),XMAX-XMIN);
  if (modval<0.0) modval += XMAX-XMIN; //make it positive
  return modval+XMIN;
}



void particles_predict(double dtin){
  int ii;

  if(isnan(Sys->x[0])) {printf("enter predict Sys->x[0] %e\n", Sys->x[0]); exit(0);}

  //printf("in particles_predict\n");
  dt = dtin; // set dt for this step, re-used by the corrector stage
  // run at the beginning of the timestep
  particles_kick(0.5*dt); // half a timestep of kick, this comes back sync'd

  // do a drift operation for full timestep length
#pragma omp parallel for 
  for (ii=0; ii<nparticles; ii++){
    if ( ownedby[ii] > 0 ) {
      // predict the end of step position based on the current velocity 
      //    (this could be fancier - using the current accel over the timestep)
      // position = position + step_time*velocity
#ifdef CARTESIAN
      posx[ii] += dt * velx[ii];
      posy[ii] += dt * vely[ii];
      posz[ii] += dt * velz[ii];
#elif defined(CYLINDRICAL)      
      posx[ii] += dt * (velx[ii] - posy[ii]*OMEGAFRAME) / posy[ii];
      posy[ii] += dt * vely[ii];
      posz[ii] += dt * velz[ii];
#elif defined(SPHERICAL)
      posx[ii] += dt * (velx[ii] - posy[ii]*OMEGAFRAME) / (posy[ii] * sin(posz[ii]));
      posy[ii] += dt * vely[ii];
      posz[ii] += dt * velz[ii] / (posy[ii]);
#else
      NOT IMPLEMENTED
#endif
    }else{
      tstop[ii] = DBL_MAX;
      acceleration_tstep[ii] = DBL_MAX;
    }
    if (acceleration_tstep[ii] < 1e-20) printf("acceleration_tstep small %e pos %e %e %e \n",
                                                acceleration_tstep[ii], posx[ii], posy[ii], posz[ii]);
  }
}


void particles_correct(){
  int ii;

  if(isnan(Sys->x[0])) {printf("enter pcorrect Sys->x[0] %e\n",Sys->x[0]); exit(0);}
  // do the second half of the kick at the end position with the end gas velocities. this comes back sync'd
  particles_kick(0.5*dt); // half a timestep of kick
  // set the particle acceleration timestep limit for the next step
  set_particle_tstep();
}


void set_particle_tstep(){
  int ii;
  particle_tstep = DBL_MAX;

  // this could be OpenMP's, it's a reduction
#pragma omp parallel for reduction(min:particle_tstep)
  for (ii=0; ii<nparticles; ii++){
    if (ownedby[ii]==CPU_Rank+1){
      particle_tstep = MIN(particle_tstep, acceleration_tstep[ii]);
      //particle_tstep = MIN(particle_tstep, tstop[ii]); // ignore tstop
      if(acceleration_tstep[ii]<1e-10) printf("acceleration_tstep small %i %i %e %e pos %e %e %e\n",
         ii,ownedby[ii],acceleration_tstep[ii],tstop[ii],posx[ii],posy[ii],posz[ii]);
    }
  }

  particle_tstep *= PARTICLE_ACC_TSTEP_FACTOR;
  MPI_Allreduce(MPI_IN_PLACE, &particle_tstep, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
}


void get_planet_accel_on_particle(int n, double px, double py, double pz, double* ax, double* ay, double* az){
  //this must be threadsafe!
  double m, x, y, z, r;
  double smoothing, dx, dy, dz, dist2, distance, accInvDist3;
  double axcart, aycart, azcart;

  //this code for force is from the torque routine in output.c mainly - should only be in one place!
  // But with how FARGO3D is written it's also in monitor routines
  m = Sys->mass[n];
  x = Sys->x[n];
  y = Sys->y[n];
  z = Sys->z[n];
  r = sqrt(x*x + y*y + z*z);
  if (ROCHESMOOTHING != 0)
    smoothing = r * pow(m/3.0/MSTAR,1./3.) * ROCHESMOOTHING;
  else
    smoothing = ASPECTRATIO * pow(r/R0,FLARINGINDEX) * r * THICKNESSSMOOTHING;
  //distances from the planet to the particle
#ifdef CARTESIAN
  dx = px - x;
  dy = py - y;
  dz = pz - z;
#endif
#ifdef CYLINDRICAL
  dx = py * cos(px) - x;
  dy = py * sin(px) - y;
  dz = pz - z;
#endif
#ifdef SPHERICAL 
  dx = py * cos(px) * sin(pz) - x;
  dy = py * sin(px) * sin(pz) - y;
  dz = py * cos(pz) - z;
#endif
  dist2 = dx * dx + dy * dy + dz * dz;
  dist2 += smoothing * smoothing;
  distance = sqrt(dist2);
  accInvDist3 = -G * m / (dist2 * distance);
  
  axcart = dx * accInvDist3;
  aycart = dy * accInvDist3;
  azcart = dz * accInvDist3;
#ifdef CARTESIAN
  *ax += axcart;
  *ay += aycart;
  *az += azcart;
#endif
#ifdef CYLINDRICAL
  // output in in cylindrical coords x->azimuthal, y->radial, z->z
  *ax += -axcart*sin(px) +aycart*cos(px);
  *ay +=  axcart*cos(px) +aycart*sin(px);
  *az +=  azcart;
#endif
#ifdef SPHERICAL
  // output in in spherical coords x->azimuthal, y->radial, z->polar 
  *ax += -axcart * sin(px) + aycart * cos(px);
  *ay +=  (axcart * cos(px) * sin(pz)) + (aycart * sin(px) * sin(pz)) + (azcart * cos(pz));
  *az +=  (axcart * cos(px) * cos(pz)) + (aycart * sin(px) * cos(pz)) - (azcart * sin(pz));
#endif
}
