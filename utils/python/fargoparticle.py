# module for reading particle log file
# Colin McNally colin@colinmcnally.ca
import numpy as np
import os
import os.path
import timeit


class particlelog:
  def __init__(self, prefix='data', coords='cylindrical'):
    self.filenametxt = os.path.join(prefix, 'particlelog.txt')
    self.filenamebin = os.path.join(prefix, 'particlelog.dat')
    self.coords = coords
    assert coords in ['cylindrical', 'spherical']

  def readbin(self):
    t0 = timeit.default_timer()
    log = open(self.filenamebin,'rb')

    if self.coords == 'cylindrical':
      binlogrecordtype = np.dtype([('time',np.float64), ('pid',np.int64), ('mass',np.float64),
                             ('radius',np.float64), ('phi',np.float64),
                             ('r',np.float64), ('z',np.float64), ('vphi',np.float64),
                             ('vr',np.float64), ('vz',np.float64), ('tstop',np.float64),
                             ('magic',np.int64)] )
    if self.coords == 'spherical':
      binlogrecordtype = np.dtype([('time',np.float64), ('pid',np.int64), ('mass',np.float64),
                             ('radius',np.float64),
                             ('theta',np.float64), ('r',np.float64), ('phi',np.float64),
                             ('vtheta',np.float64), ('vr',np.float64), ('vphi',np.float64),
                             ('tstop',np.float64), ('magic',np.int64)] )

    filesize = os.stat(self.filenamebin).st_size
    recordsize = np.ones(1, dtype=binlogrecordtype).nbytes
    nrecords = filesize//recordsize
    print('log file size ',filesize, ' record size ',recordsize, ' will read ', nrecords, ' records')
    self.plog = np.fromfile( log, dtype=binlogrecordtype, count=nrecords)
    t1 = timeit.default_timer()
    print('particlelog read took', t1-t0)

    self.pids = np.unique(self.plog['pid'])
    t2 = timeit.default_timer()
    print('particlelog pids took', t2-t1)


class particlesnap():
  def __init__(self, npart = 10):
    self.npart  = np.int64(npart)
    self.pid    = np.arange(0, npart, dtype=np.int64)
    self.mass   = np.ones(npart, dtype=np.float64)
    self.radius = np.ones(npart, dtype=np.float64)
    self.posx   = np.ones(npart, dtype=np.float64)
    self.posy   = np.ones(npart, dtype=np.float64)
    self.posz   = np.zeros(npart, dtype=np.float64)
    self.velx   = np.ones(npart, dtype=np.float64)
    self.vely   = np.ones(npart, dtype=np.float64)
    self.velz   = np.zeros(npart, dtype=np.float64)
    self.tstep  = np.ones(npart, dtype=np.float64)
    self.tstop  = np.ones(npart, dtype=np.float64)

  def read(self, filename='snapparticles_0.dat'):
    snap = open(filename, 'rb')
    self.npart  = np.fromfile(snap, dtype=np.int64, count=1)[0]
    self.pid    = np.fromfile(snap, dtype=np.int64, count=self.npart)
    self.mass   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.radius = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.posx   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.posy   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.posz   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.velx   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.vely   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.velz   = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.tstep  = np.fromfile(snap, dtype=np.float64, count=self.npart)
    self.tstop  = np.fromfile(snap, dtype=np.float64, count=self.npart)
    snap.close()

  def write(self, filename='snapparticles_0.dat'):
    snap = open(filename, 'wb')
    snap.write(np.array(self.npart, dtype=np.int64))
    snap.write(self.pid)
    snap.write(self.mass)
    snap.write(self.radius)
    snap.write(self.posx)
    snap.write(self.posy)
    snap.write(self.posz)
    snap.write(self.velx)
    snap.write(self.vely)
    snap.write(self.velz)
    snap.write(self.tstep)
    snap.write(self.tstop)

    snap.close()
