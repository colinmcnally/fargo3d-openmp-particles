from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
import matplotlib.pyplot as plt
import numpy as np
import numpy.ma as ma
import sys
import matplotlib 

import fargoparticle

# read the particle log file. Need to specify spherical coords!
pp = fargoparticle.particlelog(prefix='./', coords='spherical')

pp.readbin()

fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]
  x = p['r']*np.cos(p['theta'])*np.sin(p['phi'])
  y = p['r']*np.sin(p['theta'])*np.sin(p['phi'])
  z = p['r']*np.cos(p['phi'])

  ax.plot(x,y,linewidth=1)

ax.set_aspect('equal')
ax.set_xlim([-2,2])
ax.set_ylim([-2,2])
ax.set_xlabel('X')
ax.set_ylabel('Y')

fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]
  x = p['r']*np.cos(p['theta'])*np.sin(p['phi'])
  y = p['r']*np.sin(p['theta'])*np.sin(p['phi'])
  z = p['r']*np.cos(p['phi'])

  ax.plot(x,z,linewidth=1)

ax.set_xlim([-2,2])
ax.set_xlabel('X')
ax.set_ylabel('Z')



fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]

  ax.plot(p['time'],p['r'],linewidth=1,label=str(pid))

ax.set_xlabel('Time')
ax.set_ylabel('Radius')
ax.legend()


fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]

  ax.plot(p['r'],p['phi'],linewidth=1)


ax.set_xlabel('Radius')
ax.set_ylabel('Phi')


fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]

  ax.plot(p['time'][:-1],p['vr'][1:],linewidth=1)

ax.set_xlabel('Time')
ax.set_ylabel('vr')

fig = plt.figure()
ax = fig.add_subplot(111)

for i,pid in enumerate(pp.pids):
  p = pp.plog[np.where(pp.plog['pid']==pid)]
  ax.semilogy(p['time'],p['tstop'],linewidth=1)

ax.set_xlabel('time')
ax.set_ylabel('tstop')

plt.show()
