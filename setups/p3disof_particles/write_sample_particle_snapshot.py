import numpy as np

# This module is in fargo3d-cm/python. It provides an I/O routine for particle snapshots
import fargoparticle

# The drag law has physical units, and I've coded it in msun, au, year=2*pi units
silicate_density = 5.05e6 # 3 g/cm^3 in msun/au^3
au_in_cm = 1.496e13 #cm

# Initialize a snapshot object with 20 particles. Fills all the properties with 
# default values.
sp = fargoparticle.particlesnap(10)

# y is radius
# x is azimuth
# z is polar angle 
sp.posy = 1.02*sp.posy
sp.posx = np.pi/2 * sp.posx
sp.posz = np.pi/2 + sp.posz - 0.02*np.pi

# particle radius, drag law  assumes spheres
sp.radius = sp.radius*np.logspace(0.9, 1.4, sp.npart)/au_in_cm
# particle mass
sp.mass = silicate_density * 4.0/3.0 * np.pi * sp.radius**3 

# absolute azimuthal velocity is used in the particle module
sp.velx = sp.posy * np.sqrt(1.0 / sp.posy**3) 
sp.vely = 0.0*sp.vely # default is 1.0, soo zero it out
#velz is defaul 0.0

# set these as small, they get rest on the beginning of the first step anyways. 
# This just makes the plot a bit nicer
sp.tstop = 1e-20 * sp.tstop
sp.tstep = 1e-10 * sp.tstep

# snapshot 0 to start at the beginning of the run
filename = 'snapparticles_0.dat'
sp.write(filename)
